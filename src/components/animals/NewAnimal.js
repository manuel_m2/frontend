
import React from 'react';
import { Row, Col, message} from 'antd';
import {
  Link,
  useHistory
} from "react-router-dom";
import AnimalForm from "./AnimalForm"
import {createAnimal} from "../../services/animals"

export default function NewAnimal() {
  let history = useHistory();

  const saveAnimal = async (data) => {
    const {status} = await createAnimal(data)
    
    if(status === 200){
      message.info('Animal record created');
      history.push("/animals");
    }else{
      message.info('Server Error');
    }
  }

  return (
    <Row gutter={[16, 16]}>
      <Col span={24}>
        <h4>Create New animal</h4>
        <Link to="/animals">Back</Link>
      </Col>
      <Col span={24}>
        <AnimalForm onFinish={(values) => saveAnimal(values)}/>
      </Col>
    </Row>
  )
}