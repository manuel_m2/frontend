
import React, { useState, useEffect } from 'react';
import { Row, Col, Table, Button, message } from 'antd';
import {
  Link
} from "react-router-dom";
import {getAnimals, removeAnimal} from "../../services/animals"

export default function AnimalList() {
  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Color',
      dataIndex: 'color',
      key: 'color',
    },
    {
      title: 'Age',
      dataIndex: 'age',
      key: 'age',
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <Button onClick={() => deleteAnimal(record._id)}>Delete</Button>
      ),
    },
  ];

  const [animals, setAnimals] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const {data, status} = await getAnimals()

      if(status === 200){
        const newAnimals = data.Animals.map(item => ({...item, key: item._id}))
        setAnimals(newAnimals)
      }
    }

    fetchData();
  }, []);

  const deleteAnimal = async (id) => {
    const {data, status} = await removeAnimal(id)
    if(data.successfull === true && status === 200){
      const updatedAnimals = animals.filter(item => item._id !== id);
      setAnimals(updatedAnimals)

      message.info('Animal record deleted');
    }else{
      message.info('Server Error');
    }
  }

  return (
    <Row gutter={[16, 16]}>
      <Col span={24}>
        <Link to="/animals/new">New Animal</Link>
      </Col>
      <Col span={24}>
        <Table dataSource={animals} columns={columns} />
      </Col>
    </Row>
  )
}