
import React from 'react';
import { Row, Col, Form, Input, Button } from 'antd';

export default function AnimalForm({onFinish}) {

  return (
    <Form
    name="basic"
    onFinish={onFinish}
  >
    <Row gutter={[16, 16]}>
      <Col span={12}>
        <Form.Item
          label="Name"
          name="name"
          rules={[{ required: true, message: 'Please input the animal name' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Description"
          name="description"
          rules={[{ required: true, message: 'Please input the animal description' }]}
        >
          <Input />
        </Form.Item>
      </Col>

      <Col span={12}>
        <Form.Item
          label="Color"
          name="color"
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Age"
          name="age"
        >
          <Input type="number" />
        </Form.Item>
      </Col>

      <Col span={24}>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Col>
    </Row>
  </Form>
  )
}