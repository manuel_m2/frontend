import axios from "axios"

export const getAnimals = async () => {
  return axios.get('http://localhost:5000/api/animals')
}

export const createAnimal = async (data) => {
  return axios.post('http://localhost:5000/api/animals', data)
}

export const removeAnimal = async (id) => {
  return axios.delete(`http://localhost:5000/api/animals/${id}`)
}