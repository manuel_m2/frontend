import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import AnimalList from "./components/animals/AnimalList"
import NewAnimal from "./components/animals/NewAnimal"
import 'antd/dist/antd.css';
import './index.css';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/animals">
          <AnimalList />
        </Route>
        <Route path="/animals/new">
          <NewAnimal />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
